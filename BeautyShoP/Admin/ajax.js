function createAjaxObj() {
	obj= false;
	if(window.ActiveXObject){
		obj= new ActiveXObject('Microsoft.XMLHTTP');
	}
	else 
	if(window.XMLHttpRequest){
		obj= new XMLHttpRequest();
	}
	else {
		alert("No Ajax Supported");
	}
	return obj;
}

function getData(url,method,target){
	httpObj = createAjaxObj();
	httpObj.open(method,url,true);
	httpObj.onreadystatechange = function() {
		// alert(httpObj.readyState);
		if(httpObj.readyState == 4) {
			document.getElementById(target).innerHTML = httpObj.responseText;
		}
		else {
			document.getElementById(target).innerHTML = '<img src="loading.gif">';
		}
	}
httpObj.send(null);
}

function getXML(url,method,target) {
	httpObj = createAjaxObj();
	httpObj.open(method,url,true);
	httpObj.onreadystatechange = function() {
		if(httpObj.readyState == 4){
			document.getElementById(target).innerHTML = httpObj.responseXML;
		}
		else {
			document.getElementById(target).innerHTML = '<img src="loading.gif">';
		}
	}
	httpObj.send(null);
}

function searchProducts(src,tar) {
	var searchTerm = document.getElementById(src).value;
	var url = 'searchProducts.php?search='+searchTerm;
	getData(url,'GET',tar);
}
